package com.sam.macharia.miniexercise1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       /* final Button act3 = findViewById(R.id.activity3);
        act3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                setActivity3(v);
            }
        });*/


    }

    public void setActivity2 (View activity2){
        Intent intent = new Intent(this, Activity2.class);
        startActivity(intent);

    }

    public void setActivity3 (View activity3){
        Intent intent = new Intent(this, Activity3.class);
        startActivity(intent);

    }

    public void setActivity4 (View activity4){
        Intent intent = new Intent(this, Activity4.class);
        startActivity(intent);

    }



}
